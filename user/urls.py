from rest_framework.routers import SimpleRouter

from user.views import UsersViewset

users_routes = SimpleRouter()

users_routes.register(r"users", UsersViewset)

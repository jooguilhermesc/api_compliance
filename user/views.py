from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework.permissions import IsAdminUser
from user.serializer import UserSerializer
from rest_framework import viewsets
# Create your views here.

class UsersViewset(viewsets.ModelViewSet):
    queryset = User.objects.filter(is_staff=False)
    serializer_class = UserSerializer
    permission_classes = [IsAdminUser]

from django.shortcuts import render
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated
from compliance.serializer import GenericModelSerializer
# Create your views here.

class GenericViewset(mixins.ListModelMixin, viewsets.GenericViewSet):
    def dispatch(self, request, *args, **kwargs):
        self.model = kwargs.pop('model')
        #self.permission_classes = [IsAuthenticated]
        self.queryset = self.model.objects.using("pbconexao").all()
        serializer = GenericModelSerializer
        serializer.Meta.model = self.model
        self.serializer_class = serializer
        return super().dispatch(request, *args, **kwargs)
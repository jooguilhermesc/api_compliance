# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Acordaos(models.Model):
    id = models.IntegerField(db_column='id', primary_key=True)  # Field renamed because of name conflict.
    acordao = models.TextField(db_column='acordao', blank=True, null=True)  # Field renamed because of name conflict.
    advogado = models.TextField(db_column='advogado', blank=True, null=True)  # Field renamed because of name conflict.
    anoacordao = models.TextField(db_column='anoacordao', blank=True, null=True)  # Field renamed because of name conflict.
    assunto = models.TextField(db_column='assunto', blank=True, null=True)  # Field renamed because of name conflict.
    colegiado = models.TextField(db_column='colegiado', blank=True, null=True)  # Field renamed because of name conflict.
    dadosmateriais = models.TextField(db_column='dadosmateriais', blank=True, null=True)  # Field renamed because of name conflict.
    dadosretificacao = models.TextField(db_column='dadosretificacao', blank=True, null=True)  # Field renamed because of name conflict.
    datasessao = models.TextField(db_column='datasessao', blank=True, null=True)  # Field renamed because of name conflict.
    decisao = models.TextField(db_column='decisao', blank=True, null=True)  # Field renamed because of name conflict.
    declaracaovoto = models.TextField(db_column='declaracaovoto', blank=True, null=True)  # Field renamed because of name conflict.
    dtatualizacao = models.TextField(db_column='dtatualizacao', blank=True, null=True)  # Field renamed because of name conflict.
    entidade = models.TextField(db_column='entidade', blank=True, null=True)  # Field renamed because of name conflict.
    extensaoarquivo = models.TextField(db_column='extensaoarquivo', blank=True, null=True)  # Field renamed because of name conflict.
    interessados = models.TextField(db_column='interessados', blank=True, null=True)  # Field renamed because of name conflict.
    key_acordao = models.TextField(db_column='key_acordao', blank=True, null=True)  # Field renamed because of name conflict.
    ministroalegouimpedimentosessao = models.TextField(db_column='ministroalegouimpedimentosessao', blank=True, null=True)  # Field renamed because of name conflict.
    ministroautorvotovencedor = models.TextField(db_column='ministroautorvotovencedor', blank=True, null=True)  # Field renamed because of name conflict.
    ministrorevisor = models.TextField(db_column='ministrorevisor', blank=True, null=True)  # Field renamed because of name conflict.
    numacordao = models.TextField(db_column='numacordao', blank=True, null=True)  # Field renamed because of name conflict.
    numata = models.TextField(db_column='numata', blank=True, null=True)  # Field renamed because of name conflict.
    proc_acordao = models.TextField(db_column='proc_acordao', blank=True, null=True)  # Field renamed because of name conflict.
    quorum = models.TextField(db_column='quorum', blank=True, null=True)  # Field renamed because of name conflict.
    recursos = models.TextField(db_column='recursos', blank=True, null=True)  # Field renamed because of name conflict.
    relator = models.TextField(db_column='relator', blank=True, null=True)  # Field renamed because of name conflict.
    relatordeliberacaorecorrida = models.TextField(db_column='relatordeliberacaorecorrida', blank=True, null=True)  # Field renamed because of name conflict.
    relatorio = models.TextField(db_column='relatorio', blank=True, null=True)  # Field renamed because of name conflict.
    representantemp = models.TextField(db_column='representantemp', blank=True, null=True)  # Field renamed because of name conflict.
    situacao = models.TextField(db_column='situacao', blank=True, null=True)  # Field renamed because of name conflict.
    sumario = models.TextField(db_column='sumario', blank=True, null=True)  # Field renamed because of name conflict.
    tipo = models.TextField(db_column='tipo', blank=True, null=True)  # Field renamed because of name conflict.
    tipoprocesso = models.TextField(db_column='tipoprocesso', blank=True, null=True)  # Field renamed because of name conflict.
    titulo = models.TextField(db_column='titulo', blank=True, null=True)  # Field renamed because of name conflict.
    unidadetecnica = models.TextField(db_column='unidadetecnica', blank=True, null=True)  # Field renamed because of name conflict.
    urlarquivo = models.TextField(db_column='urlarquivo', blank=True, null=True)  # Field renamed because of name conflict.
    urlarquivopdf = models.TextField(db_column='urlarquivopdf', blank=True, null=True)  # Field renamed because of name conflict.
    voto = models.TextField(db_column='voto', blank=True, null=True)  # Field renamed because of name conflict.
    votocomplementar = models.TextField(db_column='votocomplementar', blank=True, null=True)  # Field renamed because of name conflict.
    votoministrorevisor = models.TextField(db_column='votoministrorevisor', blank=True, null=True)  # Field renamed because of name conflict.
    primeirotopico = models.IntegerField(db_column='primeirotopico', blank=True, null=True)  # Field renamed because of name conflict.
    segundotopico = models.IntegerField(db_column='segundotopico', blank=True, null=True)  # Field renamed because of name conflict.
    terceirotopico = models.IntegerField(db_column='terceirotopico', blank=True, null=True)  # Field renamed because of name conflict.
    sistema_s = models.TextField(db_column='sistema_s', blank=True, null=True)  # Field renamed because of name conflict.
    dt_carga = models.DateTimeField(db_column='dt_carga')  # Field renamed because of name conflict.
    rlc_associado = models.IntegerField(db_column='rlc_associado', blank=True, null=True)  # Field renamed because of name conflict.
    rlc_similaridade = models.FloatField(db_column='rlc_similaridade', blank=True, null=True)  # Field renamed because of name conflict.

    class Meta:
        managed = False
        db_table = 'fontes_externas].[tcu_acordaos'

class Rlc(models.Model):
    id = models.IntegerField(primary_key=True)
    instituição = models.CharField(max_length=50, blank=True, null=True)
    tipo = models.CharField(max_length=50, blank=True, null=True)
    titulo_capitulo = models.TextField(blank=True, null=True)
    num_capitulo = models.CharField(max_length=5, blank=True, null=True)
    titulo_secao = models.CharField(max_length=50, blank=True, null=True)
    num_secao = models.CharField(max_length=5, blank=True, null=True)
    num_art = models.CharField(max_length=5, blank=True, null=True)
    num_para = models.CharField(max_length=50, blank=True, null=True)
    num_inci = models.CharField(max_length=5, blank=True, null=True)
    num_alin = models.CharField(max_length=5, blank=True, null=True)
    texto = models.TextField(blank=True, null=True)
    comentario = models.TextField(blank=True, null=True)
    dt_carga = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'fontes_externas].[rlc'


class RlcAcordao(models.Model):
    id = models.IntegerField(primary_key=True)
    id_rlc = models.IntegerField(blank=True, null=True)
    key_acordao = models.TextField(blank=True, null=True)
    posicao = models.IntegerField(blank=True, null=True)
    similaridade = models.FloatField(blank=True, null=True)
    numacordao = models.TextField(blank=True, null=True)
    datasessao = models.TextField(blank=True, null=True)
    proc_acordao = models.TextField(blank=True, null=True)
    entidade = models.TextField(blank=True, null=True)
    sistema_s = models.TextField(blank=True, null=True)
    dt_carga = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'fontes_externas].[tcu_rlc_acordao'